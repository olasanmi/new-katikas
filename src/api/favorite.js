import axios from 'axios'

import { domain } from './data'

var url = domain

export function add (data, callBack, errorCallBack) {
  axios({ url: url + '/api/favorite', data: data, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors)
      }
    })
}

export function get (data, callBack, errorCallBack) {
  axios({ url: url + '/api/favorite/get', data: data, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors)
      }
    })
}

export function deleteFavorite (data, callBack, errorCallBack) {
  axios({ url: url + '/api/favorite/deletes', data: data, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors)
      }
    })
}

export function contactMail (data, callBack, errorCallBack) {
  axios({ url: url + '/api/contact', data: data, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors)
      }
    })
}
