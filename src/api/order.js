import axios from 'axios'

import { domain } from './data'

var url = domain

export function getToken (data, callBack, errorCallBack) {
  axios({ url: url + '/api/generate/token', data: data, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors)
      }
    })
}

export function createCustomer (data, callBack, errorCallBack) {
  axios({ url: url + '/api/generate/customer', data: data, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors)
      }
    })
}

export function pay (data, callBack, errorCallBack) {
  axios({ url: url + '/api/pay/order', data: data, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors)
      }
    })
}
