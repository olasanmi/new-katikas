import axios from 'axios'

import { domain } from './data'

var url = domain

export function getBanners (callBack, errorCallBack) {
  axios({ url: url + '/api/banner', method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors)
      }
    })
}

export function getBannerSecond (callBack, errorCallBack) {
  axios({ url: url + '/api/banner-seconds/getBannerActive', method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors)
      }
    })
}
