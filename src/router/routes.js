
const routes = [
  {
    path: '/',
    component: () => import('layouts/main.vue'),
    children: [
      { path: '', component: () => import('pages/index.vue') }
    ]
  },
  {
    path: '/main',
    component: () => import('layouts/main/main.vue'),
    children: [
      { path: '', component: () => import('pages/main/index.vue') },
      { path: 'product/filter', component: () => import('pages/main/filterProduct.vue') },
      { path: 'cart', component: () => import('pages/main/cart.vue') },
      { path: 'favorite', component: () => import('pages/main/favorite.vue') },
      { path: 'contact', component: () => import('pages/main/contact.vue') },
      { path: 'product', component: () => import('pages/main/product.vue') },
      { path: 'payment', component: () => import('pages/main/payment.vue') },
      { path: 'pay/order', component: () => import('pages/main/payOrder.vue') },
      { path: 'terminos', component: () => import('pages/main/terms.vue') },
      { path: 'politicas', component: () => import('pages/main/politicas.vue') },
      { path: 'comprar', component: () => import('pages/main/compra.vue') },
      { path: 'offerts', component: () => import('pages/main/offerts.vue') }
    ]
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/error404.vue')
  }
]

export default routes
