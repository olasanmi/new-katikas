import { getToken, createCustomer, pay } from 'src/api/order'

export function addItemToCart ({ commit }, data) {
  return new Promise((resolve, reject) => {
    commit('addItemToCart', data)
    resolve(data)
  })
}

export function setOrderTotal ({ commit }, data) {
  return new Promise((resolve, reject) => {
    commit('setTotal', data)
    resolve(data)
  })
}

export function deleteItem ({ commit }, data) {
  return new Promise((resolve, reject) => {
    commit('deleteItem', data)
    resolve(data)
  })
}

export function addToItem ({ commit }, data) {
  return new Promise((resolve, reject) => {
    commit('addToItem', data)
    resolve(data)
  })
}

export function deleteToItem ({ commit }, data) {
  return new Promise((resolve, reject) => {
    commit('deleteToItem', data)
    resolve(data)
  })
}

export function setCoupon ({ commit }, data) {
  return new Promise((resolve, reject) => {
    commit('setCoupon', data)
    resolve(data)
  })
}

export function setOrder ({ commit }, data) {
  return new Promise((resolve, reject) => {
    commit('setOrders', data)
    resolve(data)
  })
}

export function setFinishOrder ({ commit }, data) {
  return new Promise((resolve, reject) => {
    commit('setFinishOrders', data)
    resolve(data)
  })
}

export function generateToken ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return getToken(data, (response) => {
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function customer ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return createCustomer(data, (response) => {
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function payment ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return pay(data, (response) => {
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function changePriceInCart ({ commit }, data) {
  return new Promise((resolve, reject) => {
    commit('changePriceInCart', data)
    resolve(data)
  })
}
