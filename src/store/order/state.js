export default function () {
  return {
    quantity: 0,
    order: {},
    cart: [],
    total: 0,
    finishOrder: {}
  }
}
