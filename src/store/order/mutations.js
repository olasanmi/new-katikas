export function addItemToCart (order, data) {
  var search = order.cart.filter(result => {
    return parseInt(result.id) === parseInt(data.id)
  })

  if (search.length > 0) {
    for (var i = 0; i < order.cart.length; i++) {
      if (order.cart[i].id === data.id) {
        order.cart[i].quantity = order.cart[i].quantity + data.quantity
      }
    }
  } else {
    order.cart.push(data)
  }
}

export function setTotal (order, data) {
  order.total = data
}

export function deleteItem (order, data) {
  order.cart.splice(data.idx, 1)
}

export function addToItem (order, data) {
  order.cart[data.idx].quantity = data.quantity
}

export function deleteToItem (order, data) {
  order.cart[data.idx].quantity = data.quantity
}

export function changePriceInCart (order, data) {
  order.cart[data.idx].product.price = data.price
}

export function setCoupon (order, data) {
  order.order.coupon = data
}

export function setOrders (order, data) {
  order.order = data
}

export function setFinishOrders (order, data) {
  order.finishOrder = data
}
