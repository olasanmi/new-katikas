export function cart (order) {
  return order.cart
}

export function total (order) {
  return order.total
}

export function order (order) {
  return order.order
}

export function finishOrder (order) {
  return order.finishOrder
}
