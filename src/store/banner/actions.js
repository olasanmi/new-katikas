import { getBanners, getBannerSecond } from 'src/api/banner'

export function getBan ({ commit }) {
  return new Promise((resolve, reject) => {
    return getBanners((response) => {
      commit('setBanner', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function getBanSecond ({ commit }) {
  return new Promise((resolve, reject) => {
    return getBannerSecond((response) => {
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}
