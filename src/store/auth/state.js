export default function () {
  return {
    code: localStorage.getItem('code') || '',
    drawerRight: false
  }
}
