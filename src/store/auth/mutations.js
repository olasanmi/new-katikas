export function setCode (auth, data) {
  auth.code = data
}

export function toggleDrawerRight (auth, data) {
  auth.drawerRight = !auth.drawerRight
}
