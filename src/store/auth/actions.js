export function setCode ({ commit }, data) {
  return new Promise((resolve, reject) => {
    localStorage.removeItem('code')
    localStorage.setItem('code', data)
    commit('setCode', data)
    resolve(data)
  })
}

export function toggleDrawer ({ commit }) {
  return new Promise((resolve, reject) => {
    commit('toggleDrawerRight')
  })
}
