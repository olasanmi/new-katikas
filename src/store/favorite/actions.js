import { add, get, deleteFavorite, contactMail } from 'src/api/favorite'

export function addFavorite ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return add(data, (response) => {
      resolve(response)
      commit('addFavorite', response.data.success.data)
    }, (err) => {
      reject(err)
    })
  })
}

export function contact ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return contactMail(data, (response) => {
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function getUserFavorite ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return get(data, (response) => {
      resolve(response)
      commit('addFavorite', response.data.success.data)
    }, (err) => {
      reject(err)
    })
  })
}

export function deleteUserFavorite ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return deleteFavorite(data, (response) => {
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}
