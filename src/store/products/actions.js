export function setProducts ({ commit }, data) {
  return new Promise((resolve, reject) => {
    commit('setProducts', data)
    resolve(data)
  })
}

export function setProducts2 ({ commit }, data) {
  return new Promise((resolve, reject) => {
    commit('setProducts2', data)
    resolve(data)
  })
}

export function setProductsFilter ({ commit }, data) {
  return new Promise((resolve, reject) => {
    commit('setProductsFilter', data)
    resolve(data)
  })
}

export function clearProduct ({ commit }, data) {
  return new Promise((resolve, reject) => {
    commit('clearProduct')
    resolve(data)
  })
}
