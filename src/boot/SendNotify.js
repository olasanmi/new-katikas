import SendNotify from 'src/utilities/Notify'
import Vue from 'vue'

export default async () => {
  Vue.prototype.$SendNotify = SendNotify
}
