import PushRoute from 'src/utilities/RouterPush'
import Vue from 'vue'

export default () => {
  Vue.prototype.$pushRoute = PushRoute
}
